/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import {rhythm, scale} from "../utils/typography";

export default class extends React.Component {

    render() {
        const {header} = this.props

        return (<header>
            <p>POPISEK: {header.description}</p>
            <h1
                style={{
                    marginTop: rhythm(1),
                    marginBottom: 0,
                }}
            >
                {header.title}
            </h1>
            <p
                style={{
                    ...scale(-1 / 5),
                    display: `block`,
                    marginBottom: rhythm(1),
                }}
            >
                {header.date}
            </p>
        </header>)
    }
}
