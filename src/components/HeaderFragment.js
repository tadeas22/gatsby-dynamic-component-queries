/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

module.exports = `
fragment HeaderFragment on MarkdownRemarkFrontmatter { 
    date
    title
    description
}
`