import React from "react"
import Bio from "../components/bio"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Header from "../components/Header";

export default ({ pageContext, location }) => {
  const post = pageContext.blog

  return (
    <Layout location={location} title="sads">
      <SEO
        title={post.frontmatter.title}
        description={post.excerpt}
      />
      <article>
        <Header header={post.Header_header} />
        <section dangerouslySetInnerHTML={{ __html: post.html }} />

        <footer>
          <Bio />
        </footer>
      </article>
    </Layout>
  )
}