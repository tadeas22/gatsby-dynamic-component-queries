const fragmentHeader = require("../components/HeaderFragment");

module.exports = `
${fragmentHeader}

fragment BlogPostFragment on MarkdownRemark {
    id
    excerpt(pruneLength: 160)
    html
    Header_header: frontmatter {
        ...HeaderFragment
    }
    frontmatter {
        title
    }
 } 
`